<?php

namespace App\Http\Controllers;

use App\Repositories\CategoriesRepositoryInterface;
use App\Services\TreeService;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $categories;
    protected $treeService;

    public function __construct(CategoriesRepositoryInterface $categoriesRepository, TreeService $treeService)
    {
        $this->categories = $categoriesRepository;
        $this->treeService = $treeService;
    }

    public function index(){
        $categories = $this->categories->all();
        return response()->json($this->treeService->getTreeStructure($categories));
    }
}
