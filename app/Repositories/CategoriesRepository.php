<?php

namespace App\Repositories;


use App\Models\Categories;

class CategoriesRepository implements CategoriesRepositoryInterface
{
    protected $category;

    public function __construct(Categories $categories)
    {
        $this->category = $categories;
    }

    public function all()
    {
        return $this->category->all();
    }
}