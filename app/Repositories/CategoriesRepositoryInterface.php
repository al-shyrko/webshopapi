<?php

namespace App\Repositories;


interface CategoriesRepositoryInterface
{
    public function all();
}