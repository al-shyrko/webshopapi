<?php

namespace App\Services;


class TreeService
{
    /**
     * Prepare tree structure.
     * @param $branches - eloquent collection. Collection must be consist of elements which have 'id' and 'parent_id' fields
     * @param null $id
     * @return array
     */
    public function getTreeStructure($branches, $id = null){
        $result = [];
        $childs = [];
        foreach ($branches as $branch) {
            $childs[$branch['parent_id']][] = $branch;
        }
        if (array_key_exists($id, $childs)){
            $result['data'] = self::createTree($childs, $childs[$id], 'data');
        }
        return $result;
    }

    /**
     * Create tree structure
     * @param array $list - children
     * @param $parent - parent
     * @param string $childrenArrayName -name for childrens' array
     * @return array
     */
    private function createTree(array $list, $parent, $childrenArrayName = 'data')
    {
        $tree = [];
        foreach ($parent as $key => $value) {
            if (array_key_exists($value['id'], $list)) {
                $value[$childrenArrayName] = self::createTree($list, $list[$value['id']], $childrenArrayName);
            }else {
                $value[$childrenArrayName] = [];
            }
            $tree[] = $value;
        }
        return $tree;
    }

}