<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    const STATUS_DELIVERED = 'delivered';
    const STATUS_NEW = 'new';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_CANCELED = 'canceled';

    protected $fillable = [
        'status',
        'delivery_info_id'
    ];
}
