<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryInfos extends Model
{
    protected $fillable = [
        'client_name',
        'phone',
        'delivery_address'
    ];
}
