<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Goods extends Model
{
    protected $fillable = [
        'category',
        'name',
        'description',
        'price',
        'amount',
        'availability'
    ];

}
