<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsOrders extends Model
{
    protected $fillable = [
        'good_id',
        'order_id',
        'good_amount'
    ];
}
